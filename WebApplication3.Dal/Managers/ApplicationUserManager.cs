﻿using Microsoft.AspNet.Identity;
using WebApplication3.Entities.Identity;

namespace WebApplication3.Dal.Managers
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }
    }
}